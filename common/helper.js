const _ = require("lodash");

function isEmptyObject(obj) {
  return obj == null || !Object.keys(obj).length;
}

function isEmptyArray(arr) {
  return arr == null || !arr.length;
}

function hasKey(obj, key) {
  return _.has(obj, key) && obj[key] != null;
}

module.exports = {
  isEmptyObject,
  isEmptyArray,
  hasKey,
};
