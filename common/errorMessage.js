function errorReturn(params) {
	const{message}=params
  return {
    error: true,
    message
  };
} 
module.exports = {
  errorReturn,
};
