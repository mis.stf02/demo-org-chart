const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-org:position-routes");
const positionModule = require("../modules/positionModule");

router.get("/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await positionModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);

    const result = await positionModule.create(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await positionModule.update(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await positionModule.destroy(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
