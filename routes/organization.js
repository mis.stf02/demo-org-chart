const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-org:organization-routes");
const organizationModule = require("../modules/organizationModule");

router.get("/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await organizationModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
router.get("/chart/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await organizationModule.chart(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);

    const result = await organizationModule.create(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await organizationModule.update(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await organizationModule.destroy(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
