const express = require("express");
const router = express.Router();
const debug = require("debug")("backend-org:employee-routes");
const employeeModule = require("../modules/employeeModule");

router.get("/", async (req, res) => {
  try {
    const payload = req.query;
    debug(payload);
    let result = {};
    result = await employeeModule.read(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.post("/", async (req, res) => {
  try {
    const payload = req.body;
    debug(payload);

    const result = await employeeModule.create(payload);
    debug(result);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.put("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await employeeModule.update(payload);
    debug(payload);
    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});

router.delete("/", async (req, res) => {
  try {
    const payload = req.body;
    const result = await employeeModule.destroy(payload);

    if (!result.error) {
      return res.send(result);
    } else {
      return res.status(400).send(result);
    }
  } catch (err) {
    debug(err);
    return res.status(500).send(err);
  }
});
module.exports = router;
