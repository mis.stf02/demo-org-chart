const mongoose = require("mongoose");

const Organization = mongoose.model(
  "Organization",
  new mongoose.Schema({
    parent_id: { type: String },
    position_id: { type: String, required: true },
    is_active: { type: Boolean },
  }),
  "organization"
);
module.exports = Organization;
