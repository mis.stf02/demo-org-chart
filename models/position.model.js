const mongoose = require("mongoose");

const Position = mongoose.model(
  "Position",
  new mongoose.Schema({
    employee_id: { type: String, required: true },
    position_code: { type: String, required: true },
    position_name: { type: String, required: true },
    is_active: { type: Boolean },
  }),
  "position"
);
module.exports = Position;
