const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.employee = require("./employee.model");
db.position = require("./position.model");
db.organization = require("./organization.model");
module.exports = db;
