const mongoose = require("mongoose");

const Employee = mongoose.model(
  "Employee",
  new mongoose.Schema({
    name: { type: String, required: true },
    is_active: { type: Boolean },
  }),
  "employee"
);
module.exports = Employee;
