require("dotenv").config();
const pjs = require("./package.json");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const debug = require("debug")("backend-org:server");
const error = require("./common/errorMessage");
const app = express();

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//mongodb
const db = require("./models");
const MONGO_URI = process.env.MONGO_CLOUD
  ? process.env.MONGO_CLOUD
  : `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;

db.mongoose
  .connect(`${MONGO_URI}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    auth: { authSource: "admin" },
    user: `${process.env.MONGO_USERNAME}`,
    pass: `${process.env.MONGO_PASSWORD}`,
  })
  .then(() => {
    debug("Successfully connect to MongoDB.");
  })
  .catch((err) => {
    debug("Connection error", err);
    process.exit();
  });

const organizationRouter = require("./routes/organization");
app.use("/api/organization", organizationRouter);
const positionRouter = require("./routes/position");
app.use("/api/position", positionRouter);
const employeeRouter = require("./routes/employee");
app.use("/api/employee", employeeRouter);

//checking health pod
app.get("/health", (req, res) => {
  res.send(`Apps is healthy`);
});
app.get("/", (req, res) => {
  res.send(`Welcome to Demo App`);
});
app.use((req, res, next) => {
  return res
    .status(404)
    .send(error.errorReturn({ message: "Invalid routes!" }));
});

// START SERVER
server = app.listen(process.env.PORT || 0, () => {
  debug(
    `${pjs.name} v${pjs.version} listening on ${server.address().address}:${
      server.address().port
    }...`
  );
});
