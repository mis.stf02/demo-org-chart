const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-org:employee-module");
const helper = require("../common/helper");
const db = require("../models");
const Employee = db.employee;

const createSchema = Joi.object({
  name: Joi.string().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  name: Joi.string().required(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}
async function create(payload) {
  try {
    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    payload.is_active = true;
    const result = await new Employee(payload).save();
    return result;
  } catch (e) {
    debug(e, "employee-module");
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    let condition = { is_active: true };

    if (query.name) {
      condition.name = new RegExp(escapeRegex(query.name), "i");
    }

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    const data = await Employee.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(data)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Employee.find(condition);
    const result = {
      foundData: data,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function update(payload) {
  try {
    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const update = { name: payload.name };
    const result = await Employee.findOneAndUpdate(
      { _id: payload._id },
      update,
      {
        useFindAndModify: false,
        new: true,
      }
    );
    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function destroy(payload) {
  try {
    const validate = validateDeleteSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let filter = { _id: payload._id };

    let result = await Employee.findOneAndUpdate(filter, {
      is_active: false,
    });

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  create,
  read,
  update,
  destroy,
};
