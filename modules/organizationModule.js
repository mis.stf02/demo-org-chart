const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-org:organization-module");
const helper = require("../common/helper");
const db = require("../models");
const Employee = require("../models/employee.model");
const Position = require("../models/position.model");
const Organization = db.organization;

const createSchema = Joi.object({
  parent_id: Joi.string().allow(null),
  position_id: Joi.string().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  parent_id: Joi.string().required(),
  position_id: Joi.string().required(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}
async function create(payload) {
  try {
    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    payload.is_active = true;
    const result = await new Organization(payload).save();
    return result;
  } catch (e) {
    debug(e, "organization-module");
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    let condition = { is_active: true };

    if (query.position_id) {
      condition.position_id = new RegExp(escapeRegex(query.position_id), "i");
    }
    if (query.parent_id) {
      condition.parent_id = new RegExp(escapeRegex(query.parent_id), "i");
    }

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    const data = await Organization.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(data)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Organization.find(condition);
    const result = {
      foundData: data,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}
async function chart() {
  try {
    const condition = {
      is_active: true,
    };
    const allData = await Organization.find(condition).lean();
    const allPos = await Position.find(condition).lean();
    const allEmp = await Employee.find(condition).lean();
    const root = allData.find((aa) => !aa.parent_id);
    if (!root) return error.errorReturn({ message: "Chart is not found" });
    // looping based on root
    const result = await recursionTree(allData, allPos, allEmp);
    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}
async function recursionTree(arr, pos, emp, item) {
  if (!item) {
    item = arr.find((item) => item.parent_id === null);
  }
  let parent = { ...item };

  if (
    typeof pos.filter((p) => p._id.toString() == parent.position_id) !==
    "undefined"
  ) {
    const curPos = pos.find((p) => p._id.toString() == parent.position_id);
    parent.position_code = curPos.position_code;
    parent.position_name = curPos.position_name;
    // get employe based on position
    const curEmploye = emp.filter(
      (e) => e._id.toString() == curPos.employee_id
    );
    parent.employee = [...curEmploye];
  }
  parent.children = await Promise.all(
    arr
      .filter((x) => {
        return x.parent_id === item._id.toString();
      })
      .sort((a, b) => a._id - b._id)
      .map(async (y) => await recursionTree(arr, pos, emp, y))
  );

  return parent;
}

async function update(payload) {
  try {
    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const update = {
      parent_id: payload.parent_id,
      position_id: payload.position_id,
    };
    const result = await Organization.findOneAndUpdate(
      { _id: payload._id },
      update,
      {
        useFindAndModify: false,
        new: true,
      }
    );
    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function destroy(payload) {
  try {
    const validate = validateDeleteSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let filter = { _id: payload._id };

    let result = await Organization.findOneAndUpdate(filter, {
      is_active: false,
    });

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  create,
  read,
  update,
  destroy,
  chart,
};
