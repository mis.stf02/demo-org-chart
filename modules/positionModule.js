const Joi = require("joi");
const error = require("../common/errorMessage");
const debug = require("debug")("backend-org:positino-module");
const helper = require("../common/helper");
const db = require("../models");
const Position = db.position;

const createSchema = Joi.object({
  employee_id: Joi.string().required(),
  position_code: Joi.string().required(),
  position_name: Joi.string().required(),
});

const updateSchema = Joi.object({
  _id: Joi.string().required(),
  employee_id: Joi.string().required(),
  position_code: Joi.string().required(),
  position_name: Joi.string().required(),
});

const deleteSchema = Joi.object({
  _id: Joi.string().required(),
});

function validateCreateSchema(schema) {
  return createSchema.validate(schema);
}

function validateUpdateSchema(schema) {
  return updateSchema.validate(schema);
}

function validateDeleteSchema(schema) {
  return deleteSchema.validate(schema);
}
async function create(payload) {
  try {
    const validate = validateCreateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    // check duplicate
    const checkDuplicate = await Position.find({
      is_active: true,
      position_code: payload.position_code,
      employee_id: payload.employee_id,
    }).lean();
    if (!helper.isEmptyObject(checkDuplicate))
      return error.errorReturn({ message: "Data is already exists" });
    payload.is_active = true;
    const result = await new Position(payload).save();
    return result;
  } catch (e) {
    debug(e, "positino-module");
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function read(query) {
  try {
    let condition = { is_active: true };

    if (query.position_code) {
      condition.position_code = new RegExp(
        escapeRegex(query.position_code),
        "i"
      );
    }
    if (query.employee_id) {
      condition.employee_id = new RegExp(escapeRegex(query.employee_id), "i");
    }

    //sort
    const sortOrder =
      query.sortOrder && query.sortOrder.toLowerCase() == "asc" ? 1 : -1;
    const sortField = query.sortBy ? query.sortBy : "created_at";
    const sort = query.sortBy ? { [sortField]: sortOrder } : { created_at: -1 };

    const data = await Position.find(condition)
      .sort(sort)
      .collation({ locale: "en_US", numericOrdering: true })
      .skip(skip)
      .limit(limit)
      .select("-__v");

    if (helper.isEmptyObject(data)) {
      return {
        foundData: [],
        currentPage: page,
        countPages: 0,
        countData: 0,
      };
    }
    const numFound = await Position.find(condition);
    const result = {
      foundData: data,
      currentPage: page,
      countPages: Math.ceil(Object.keys(numFound).length / sizePerPage),
      countData: Object.keys(numFound).length,
    };

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function update(payload) {
  try {
    const validate = validateUpdateSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }

    const update = {
      employee_id: payload.employee_id,
      position_code: payload.position_code,
      position_name: payload.position_name,
    };
    const result = await Position.findOneAndUpdate(
      { _id: payload._id },
      update,
      {
        useFindAndModify: false,
        new: true,
      }
    );
    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

async function destroy(payload) {
  try {
    const validate = validateDeleteSchema(payload);
    if (validate.error) {
      return error.errorReturn({ message: validate.error.details[0].message });
    }
    let filter = { _id: payload._id };

    let result = await Position.findOneAndUpdate(filter, {
      is_active: false,
    });

    return result;
  } catch (err) {
    debug(err);
    return error.errorReturn({ message: "Internal server error" });
  }
}

module.exports = {
  create,
  read,
  update,
  destroy,
};
